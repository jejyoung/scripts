import numpy as np
import matplotlib.pyplot as plt
from tempProfReader import readFile

temp,Nf = readFile('tempProf.dat')

# calculate average
nstart = 500 #frame to start at
nslice = temp[0].shape[0]
profile = np.zeros(nslice)
for i in range(nstart,Nf):
    profile += temp[i][:,1]
profile /= (Nf-nstart)

# plot
plt.figure()
plt.plot(temp[0][:,0], profile)
np.savetxt('profile.dat', profile)
plt.ylabel('T (K)')
plt.savefig('tempProfile.png', bbox_inches='tight')
plt.show()
