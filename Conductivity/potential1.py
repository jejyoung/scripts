import numpy as np
import matplotlib.pyplot as plt
from fileReader import readFile
from scipy.optimize import curve_fit


# Calculate charge profile
# Read in file
nstart = 50 # step to start trajectory at
tlist, nstep = readFile('potTraj.out')
natom = tlist[0].shape[0]
# Calculate velocity profile
nbin = 100 #number of bins
L = 928.5 #box length in dim direction
Lstart = 0 #box min in dim direction
charge = 0.5
bins = np.linspace(0, L, nbin)
pot = np.zeros(nbin-1)
count = np.zeros(nbin-1)
dim = 1 #1=x, 2=y, 3=z
for i in range(nstart, nstep):
    conf = tlist[i]
    # Move start to 0
    conf[:,dim] -= Lstart
    # Bin
    bindex = np.digitize(conf[:,dim],bins) - 1 #subtract 1 since bins to upper value
    # Split into types 1 and 2
    for j in range(natom):
        if conf[j,0] == 1: # atom name == 1
            pot[bindex[j]] += conf[j,4]/charge
        else:
            pot[bindex[j]] -= conf[j,4]/charge
        count[bindex[j]] += 1

# Normalize results
#pot/(nstep-nstart)/natom
pot = pot/count
pot *= 4.184 #convert to kJ/mol
np.savetxt('potential.dat', np.stack((bins[:-1],pot),axis=-1))
