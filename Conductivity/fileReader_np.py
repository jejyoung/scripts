import numpy as np

def readFile(fname):
        nPart = 0
        nskip = 9 # number of lines to skip at beginning of snapshot
        nframe = 0
        lineNum = 3 # line of file that has number of particles starting at 0
        l = 0
        traj = []
        with open(fname) as f:
            while l < lineNum:
                f.readline()
                l+=1
            # Read line that has number of particles
            nPart = int(f.readline().strip())
        while True:
            try:
                traj.append(np.loadtxt(fname, skiprows=nskip, max_rows=nPart))
                nskip += nPart + 9
                nframe += 1
            except StopIteration:
                return (traj, nframe)
