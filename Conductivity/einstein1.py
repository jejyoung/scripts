import numpy as np
import matplotlib.pyplot as plt
from readCustomFaster import readFile

L = 38.674124
charge = 0.5
data, Nf = readFile('traj.out') # Atoms must be consistently ordered
print('done reading')
Np = data[0].shape[0]
z = -(data[0][:,0]*2 -3) #Get list of charges (1*2-3 = -1, 2*2-3 = +1, switch signs)
z = z[..., np.newaxis]*charge
print(Nf)
offset = 150 #frames (300 ps)
maxTime = 500 #frames (1 ns)
ends = np.arange(maxTime,Nf+1,offset)
starts = ends-maxTime
Nframe = ends.shape[0]
count = np.zeros(maxTime)
msd = np.zeros(maxTime)
# Loop through all frames and calculate msd
for i in range(Nframe):
    traj0 = data[starts[i]]
    if i % 10 == 0:
        print(i)
    for j in range(maxTime):
        traj1 = data[starts[i]+j+1]
        # Calculate distance without pbc since coordinates unrapped.
        dist = traj1[:,1:4]-traj0[:,1:4]
        #dist = dist - np.rint(dist/L)*L
        toMult = z*dist
        msd[j] += np.sum(np.matmul(toMult,toMult.T))
        count[j] += 1
# Average
msd = msd/count
np.savetxt('msd2.dat', msd)
t = np.arange(1,maxTime)
plt.plot(t,msd)
