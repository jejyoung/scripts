import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import cumtrapz

autocorr = np.loadtxt('autocorrelation.dat')
dt = 2 #fs
cutoff = 5000 #steps
V = 38.674124E-10**3 #m3
T = 300 #K
kb = 1.38064852E-23 #J/K
e = 1.60217662E-19 #C

integral = cumtrapz(autocorr[:cutoff,1], autocorr[:cutoff,0]*dt)
integral = integral*1E-5*e**2 #Convert units to C2m2/s
cond = integral/(3*V*kb*T)
print(cond)
plt.plot(autocorr[:cutoff-1,0],cond)
plt.xlabel('timestep')
plt.ylabel('calculated conductivity (S/m)')
plt.savefig('integralConv.png', bbox_inches='tight')
plt.show()
