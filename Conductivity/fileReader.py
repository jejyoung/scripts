import numpy as np

def readFile(fname):
        nPart = 0
        nskip = 9 # number of lines to skip at beginning of snapshot
        nframe = 0
        lineNum = 0
        traj = []
        with open(fname) as f:
            for line in f:
                lineNum += 1
                if lineNum == 4:
                    # Read number of atoms
                    nPart = int(line.strip())
                elif lineNum == nskip:
                    # Read number of columns and create numpy array
                    nCol = len(line.split()) - 2
                    frame = np.zeros((nPart,nCol))
                elif lineNum == nskip+nPart+1:
                    # Append frame to list and reset to read in next snapshot
                    traj.append(frame)
                    lineNum = 1
                    nPart = 0
                    nframe += 1
                elif lineNum > nskip:
                    # Read data from line
                    frame[lineNum-nskip-1, :] = np.fromstring(line, sep=' ')
        return (traj, nframe)
