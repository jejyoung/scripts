import numpy as np

def readFile(fname):
        nLine = 0
        nskip = 1 # number of lines to skip at beginning of snapshot
        nframe = 0
        lineNum = 0
        traj = []
        with open(fname) as f:
            for line in f:
                lineNum += 1
                if line.split()[0] == '#':
                    lineNum -= 1
                elif lineNum == 1:
                    # Read number of rows
                    nLine = int(line.split()[1])
                    frame = np.zeros((nLine,2))
                elif lineNum == nskip+nLine:
                    # Read data from line
                    frame[lineNum-nskip-1, :] = np.fromstring(line, sep=' ')
                    # Append frame to list and reset to read in next snapshot
                    traj.append(frame)
                    lineNum = 0
                    nLine = 0
                    nframe += 1
                elif lineNum > nskip:
                    # Read data from line
                    frame[lineNum-nskip-1, :] = np.fromstring(line, sep=' ')
        return (traj, nframe)
