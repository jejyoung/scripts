import numpy as np
import matplotlib.pyplot as plt

# Read in file
data = np.loadtxt('data2.txt') #Order: 0step 1temp 2press 3vol 4vx+ 5vy+ 6vz+ 7vx- 8vy- 9vz-
dt = 2E-15 #s
e = 1.60217662E-19 #C
# Calculate current
z = 0.5
Nion = 1024
J = (z*data[:,4:7]-z*data[:,7:])*Nion/2
# Calculate autocorrelation function
N = J.shape[0] #number of steps
# Loop through zero points (from 0 to t/2)
tmax = 25000 #cutoff for calculation of autocorrelation
itmax = N-tmax
auto = np.zeros(tmax)
for i in range(itmax):
    auto += np.inner(J[i:i+tmax,:],J[i,:]) #perform dot product at each time
    if i % 100000 == 0:
        print(i)
# Average
auto /= itmax
np.savetxt('autocorrelation.dat', np.stack((data[:tmax,0],auto),axis=-1))

# Integrate

# plot
plt.figure()
plt.plot(data[:tmax,0],auto)
plt.xlabel('timestep')
plt.xlim(left=0)
plt.savefig('autocorr.png', bbox_inches='tight')
plt.xlim((0,3000))
plt.savefig('autocorrzoom.png', bbox_inches='tight')
plt.show()
