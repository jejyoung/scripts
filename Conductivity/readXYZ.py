import numpy as np

def readFile(fname):
        nPart = 0
        nskip = 2
        nframe = 0
        traj = []
        with open(fname) as f:
            nPart = int(f.readline().strip())
        while True:
            try:
                traj.append(np.loadtxt(fname, skiprows=nskip, max_rows=nPart))
                nskip += nPart + 2
                nframe += 1
            except StopIteration:
                return (traj, nframe)
