import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def f(x, m, b):
    return m*x + b

msd = np.loadtxt('msd2.dat')
dt = 0.002 #ps
t = np.arange(0,500000,1000)*dt

V = 38.674124E-10**3 #m3
T = 300 #K
kb = 1.38064852E-23 #J/K
e = 1.60217662E-19 #C
lamb = 1.126E-3 #S/m
exp = 6*V*kb*T*lamb #m2/s

msd = msd*e**2/1E20 #m2C2
t = t*1E-12 #s

# Fit line
fitmin = 50
fitmax = 400
fitParam, fitPerf = curve_fit(f, t[fitmin:fitmax], msd[fitmin:fitmax], [0.1,0])

# Calculate conductivity
cond = fitParam[0]/(6*V*kb*T)
print(cond)

plt.plot(t,msd)
plt.plot(t,f(t,fitParam[0], fitParam[1]),'k--')
plt.xlabel('time (s)')
plt.ylabel('RHS (m$^2$C$^2$)')
plt.savefig('msd.png', bbox_inches='tight')
plt.show()
