import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

bins, pot = np.loadtxt('potential.dat', unpack=True)

# Calculate slope
def fit (x, m, b):
    return m*x+b
yz = 29.1E-10 #m
fSwap = 100000 #timesteps
dt = 2E-15 #s
e = 1.60217662E-19 #C
charge = 0.5
Na = 6.02214E23
fitSkip = 5 #bins
Nbin = bins.shape[0]-1
fit1, fit1data = curve_fit(fit, bins[fitSkip:int(Nbin/2)-fitSkip], pot[fitSkip:int(Nbin/2)-fitSkip], [1,0])
fit2, fit2data = curve_fit(fit, bins[int(Nbin/2)+fitSkip:-fitSkip], pot[int(Nbin/2)+fitSkip:-fitSkip], [-1, 10])
j = 1/fSwap/dt*e/yz/yz*charge #C/s/m2  x2 since swap 2 charges, /2 since 2 flow directions
slope1 = fit1[0]/Na/e/charge*1E10*1000
slope2 = fit2[0]/Na/e/charge*1E10*1000
cond1 = j/slope1
cond2 = j/slope2
print(cond1)
print(cond2)

# Plot
plt.figure()
plt.plot(bins,pot,'b-')
plt.xlabel('X (A)')
plt.ylabel('Potential (kJ/mol*e)')
plt.savefig('potentialProf.png', bbox_inches='tight')
plt.show()
