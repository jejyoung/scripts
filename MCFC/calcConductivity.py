import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import cumtrapz

# Read in file
data    = np.loadtxt('current.xvg', comments=['@','#'])

# Define constants
e       = 1.60217662e-19
k       = 1.38064852e-23
T       = 923.15
V       = 3.59727**3      #Volume in nm^3 unit
cnst    = (e*e*1.0e21)/(3.0*T*V*k)

# Calculate autocorrelation function
N       = data.shape[0]
offset  = 100
maxstep = 3000
Navg    = 5
Nloop   = int(N/offset/Navg-maxstep/offset+1)
Nshift  = int(N/Navg)
auto    = np.zeros((maxstep,Navg))

print("Total correlation length to be computed =",maxstep,"ps")
print("")

for j in range(Navg):
	for i in range(Nloop):
		start = j*Nshift+i*offset
		auto[:,j] += np.inner(data[start:start+maxstep,1:], data[start,1:])

# Average
auto /= Nloop
np.savetxt('autocorrelation.dat', np.hstack((data[:maxstep,:1],auto)))

# Integrate
integral = np.zeros((maxstep-1,Navg))
for j in range(Navg):
	integral[:,j] = cumtrapz(auto[:,j], data[:maxstep,0])

sigma     = integral*cnst
sigAvg    = np.mean(sigma[1000:,:],axis=0)
AvgsigAvg = np.mean(sigAvg)
sigStd    = (np.std(sigAvg, ddof=1))/np.sqrt(Navg)

print("Conductivity from five blocks")
print(sigAvg)
print("")

print("Average conductivity =",AvgsigAvg)
print("")
print("Standard error =",sigStd)

np.savetxt('sigma.dat', np.hstack((data[:maxstep-1,:1],sigma)))

# Plot
#plt.figure()
#plt.plot(data[:offset,0],auto)
#plt.xlabel('time (ps)')
#plt.savefig('autocorr.png', bbox_inches='tight')
#plt.show()
