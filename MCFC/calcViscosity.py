import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import cumtrapz

# Read in file
data    = np.loadtxt('pres.xvg', comments=['@','#'])
print('done reading')
print('')

# Define constants
k       = 1.38064852e-23
T       = 923.15
V       = (3.59727**3)*(10**-27)    #Volume in m^3 unit
cnst    = (V*1e-9)/(10.0*T*k)   #1e-9 is to convert (Pa ps) to cP

# Calculate autocorrelation function
N       = data.shape[0]
offset  = 100 # how frequent to take starting points in # of steps
maxstep = 30000 # how long to calculate autocorrelation function
Navg    = 5 # how many blocks to take
Nloop   = int(N/offset/Navg-maxstep/offset+1)
Nshift  = int(N/Navg)
auto    = np.zeros((maxstep,Navg))

p = np.zeros((N,3,3))
# data order: time, pxx, pxy, pxz, pyy, pyz, pzz
p[:,0,:] = data[:,1:4] # pxx, pxy, pxz
p[:,1,0] = data[:,2] # pxy
p[:,1,1:3] = data[:,4:6] # pyy, pyz
p[:,2,0] = data[:,3] # pxz
p[:,2,1] = data[:,5] # pyz
p[:,2,2] = data[:,6] # pzz
# Convert units to Pa
p = p*1e5
# Remove trace
p = p - np.eye(3,3)[np.newaxis,...]*np.trace(p, axis1=1, axis2=2)[:,np.newaxis,np.newaxis]/3 #subtract 1/3 of trace from diagonals

print("Total correlation length to be computed =",maxstep,"ps")

for j in range(Navg):
        for i in range(Nloop):
                start = j*Nshift+i*offset
                end = start+maxstep
                # data order: time, pxx, pxy, pxz, pyy, pyz, pzz
                auto[:,j] += np.tensordot(p[start:end,:,:], p[start,:,:], axes=2) # compute double dot product over last two axes

# Average
auto /= Nloop
np.savetxt('autocorrelation.dat', np.hstack((data[:maxstep,:1],auto)))

# Integrate
integral = np.zeros((maxstep-1,Navg))
for j in range(Navg):
        integral[:,j] = cumtrapz(auto[:,j], data[:maxstep,0])

sigma     = integral*cnst
sigAvg    = np.mean(sigma[20000:,:],axis=0)
AvgsigAvg = np.mean(sigAvg)
sigStd    = (np.std(sigAvg, ddof=1))/np.sqrt(Navg)

print("Viscosity from five blocks")
print(sigAvg)

print("Average viscosity =",AvgsigAvg)
print("Standard error =",sigStd)

np.savetxt('visco.dat', np.hstack((data[:maxstep-1,:1],sigma)))
