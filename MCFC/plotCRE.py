import numpy as np
import matplotlib.pyplot as plt
from actFitter import ActFitter

names1 = ['Li0.4K0.6', 'Li0.5K0.5', 'Li0.6K0.4'] # compare effect of Li
names2 = ['Li0.5Na0.5', 'Li0.5K0.5', 'Na0.5K0.5'] # compare effect of cation
#names = ['Li0.4K0.6', 'Li0.5K0.5', 'Li0.6K0.4', 'Li0.5Na0.5', 'Na0.5K0.5'] # compare all


colordict = {
        'Li0.4K0.6' : 'C1',
        'Li0.5K0.5' : 'C0',
        'Li0.6K0.4' : 'C2',
        'Li0.5Na0.5' : 'C3',
        'Na0.5K0.5' : 'C5'
        }

labeldict = {
        'Li0.4K0.6' : 'Li$_{0.4}$K$_{0.6}$',
        'Li0.5K0.5' : 'Li$_{0.5}$K$_{0.5}$',
        'Li0.6K0.4' : 'Li$_{0.6}$K$_{0.4}$',
        'Li0.5Na0.5' : 'Li$_{0.5}$Na$_{0.5}$',
        'Na0.5K0.5' : 'Na$_{0.5}$K$_{0.5}$'
                }

tick = [1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0]
fig, (ax1, ax2) = plt.subplots(2,1, figsize=(6,10.5))
plt.subplots_adjust(hspace=0.15)

x = np.linspace(0.1,0.99,100)

for name in names1:
    data = np.loadtxt(name+'.dat')
    fit = ActFitter(name) #make activity coefficient fit object
    ax1.plot(data[:,2], data[:,1], 'o', color = colordict[name], label=labeldict[name])
    ax1.plot(fit.calcP(x), 1-x, '-.', color = colordict[name])
    #plt.plot(data[:,2], data[:,1]/(2*data[:,0]+data[:,1]), 'o-.', color = colordict[name], label=name)

ax1.set_xscale('log')
ax1.set(xlim=(1e-7,2.0), ylim=(0.0,1.0), ylabel='$x_{\mathrm{OH^{-}}}$')
ax1.set_xticks(tick)
ax1.legend(frameon=False, loc='center right')

for name in names2:
    data = np.loadtxt(name+'.dat')
    fit = ActFitter(name)
    ax2.plot(data[:,2], data[:,1], 'o', color = colordict[name], label=labeldict[name])
    ax2.plot(fit.calcP(x), 1-x, '-.', color = colordict[name])

ax2.set_xscale('log')
ax2.set(xlim=(1e-7,2.0), ylim=(0.0,1.0), xlabel='$P_{\mathrm{CO_{2}}}$(bar)', ylabel='$x_{\mathrm{OH^{-}}}$')
ax2.set_xticks(tick)
ax2.legend(frameon=False, loc='center right')

plt.savefig('creHydroxide.pdf', format='pdf')
plt.show()
