import numpy as np
from scipy.optimize import minimize

# Class that will fit activity coefficients
class ActFitter:
    def __init__(self, name):
        # Read data from name files
        self.x1, self.x2, self.lg1, self.lg2 = np.loadtxt(name+'ActCoeff.dat', unpack=True)
        mu = np.loadtxt(name+'Ref.dat', unpack=True)
        self.mu1 = mu[0]
        self.mu2 = mu[1]
        self.haveFit = False
        self.R = 0.0083144598
        self.T = 923.15
        self.delG_H2O = -195.347
        self.delG_CO2 = -398.947
        if name == 'Na0.5K0.5':
            self.T = 1023.15
            self.delG_H2O = -191.304
            self.delG_CO2 = -395.913

    def fitActCoeff(self):
        self.fit = minimize(self.mingamma, [.1, .1, .1, .1, .1, .1, .1, .1, .1], args=(self.x1,self.x2,self.lg1,self.lg2), method='Nelder-Mead').x
        self.haveFit = True

    def mingamma(self, params,x1,x2,g1,g2):
        return np.sum((g1-self.lng1(params,x1,x2))**2)+ np.sum((g2-self.lng2(params,x1,x2))**2)

    def lng1(self, params,x1,x2):
        g = np.zeros(x1.size)
        for i in range(x1.size):
            # reshape parameters into 3x3
            params = params.reshape((3,3))
            x = np.array([x1[i],x2[i],1-x1[i]-x2[i]]).reshape((1,3))
            g[i] = 1-np.log(np.sum(x*params[:1,:]))-np.sum(x.T*params[:,:1]/(np.sum(x*params, axis=1, keepdims=True)))
        return g

    def lng2(self, params,x1,x2):
        g = np.zeros(x1.size)
        for i in range(x1.size):
            # reshape parameters into 3x3
            params = params.reshape((3,3))
            x = np.array([x1[i],x2[i],1-x1[i]-x2[i]]).reshape((1,3))
            g[i] = 1-np.log(np.sum(x*params[1:2,:]))-np.sum(x.T*params[:,1:2]/(np.sum(x*params, axis=1, keepdims=True)))
        return g

    def calcP(self, x_car):
        if not self.haveFit:
            self.fitActCoeff()
        xconv_car = 1/(2*(1-x_car)/x_car+3)
        xconv_hyd = (1-3*xconv_car)/2
        RT = self.R*self.T
        mu_car = self.mu1 + RT*np.log(xconv_car) + RT*self.lng1(self.fit, xconv_car, xconv_hyd)
        mu_hyd = self.mu2 + RT*np.log(xconv_hyd) + RT*self.lng2(self.fit, xconv_car, xconv_hyd)
        mu_wat = self.delG_H2O + RT*np.log(0.1)
        return np.exp((mu_wat + mu_car - 2*mu_hyd - self.delG_CO2)/(RT))


