import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt

# Calculate the uncertainty in the fit
# Input: Lname.dat, mdCname.dat mdVname.dat gcmcname.dat
# Output: Fit parameters and figure nameExtrap.png
# Fit: u = A/L + uInf (for MD)
#      u = A/L + B/L^3 + unInf (for GCMC)


# Fitting function
# Parameter order: [Infinite dilution mu, slope md Conducting, slope md Vacuum, limiting slope GCMC, 1/L^3 slope GCMC]
def mdCFit(params, x):
    return params[1]*x + params[0]
def mdVFit(params, x):
    return params[2]*x + params[0]
def gcmcCFit(params, x):
    return params[3]*x + params[4]*x**3 + params[0]
def toMinimize(params, L, mdC, mdV, gcmcC):
    return np.sum((mdC-mdCFit(params, 1/L[:4]))**2) + np.sum((mdV-mdVFit(params, 1/L[:3]))**2) + np.sum((gcmcC-gcmcCFit(params, 1/L))**2)

# Name for files
nameList = ['006m', '02m', '03m', '07m', '14m', '34m', '67m']

plt.figure(figsize=(8, 12))
# Loop through all names
for name in nameList:
    # Data
    L = np.loadtxt('L'+name+'.dat', unpack=True)
    mdCond, mdCondUncer = np.loadtxt('mdC'+name+'.dat', unpack=True)
    mdVac, mdVacUncer = np.loadtxt('mdV'+name+'.dat', unpack=True)
    gcmcCond, gcmcCondUncer = np.loadtxt('gcmc'+name+'.dat', unpack=True)

    # Fit once to determine parameter guesses
    guess = minimize(toMinimize, [-77, -3, -2, -3, 300], args = (L, mdCond, mdVac, gcmcCond), method = 'Nelder-Mead').x


    # Loop through Nsamples
    Nsample = 1000
    fit = np.zeros((Nsample, 5))
    # Array of x for line uncertainty
    x3 = np.linspace(0, 0.25, 100)
    mdCondLineUncer = np.zeros((Nsample, x3.size))
    mdVacLineUncer = np.zeros((Nsample, x3.size))
    gcmcLineUncer = np.zeros((Nsample, x3.size))
    for i in range(Nsample):
        # Randomly select dataset
        mdC = np.random.normal(mdCond, mdCondUncer)
        mdV = np.random.normal(mdVac, mdVacUncer)
        gcmcC = np.random.normal(gcmcCond, gcmcCondUncer)
        # Calculate parameter values
        param = minimize(toMinimize, guess, args = (L, mdC, mdV, gcmcC), method = 'Nelder-Mead').x
        # Add parameters to list
        fit[i,:] = param
        # Add fit to line uncertainty
        mdCondLineUncer[i,:] = mdCFit(param, x3)
        mdVacLineUncer[i,:] = mdVFit(param, x3)
        gcmcLineUncer[i,:] = gcmcCFit(param, x3)

    # Calculate average and standard deviation
    averageFit = np.average(fit, axis=0)
    stDevFit = np.std(fit, axis=0, ddof=1)
    np.set_printoptions(linewidth=100, suppress=True)
    print("The average is " + repr(averageFit))
    print("The standard deviation is " + repr(stDevFit))
    print("The guess was " + repr(guess))

    # Plot output
    x = 1/L
    x2 = np.array((0,0.25))
    # Create lines
    mdCondLine = mdCFit(averageFit, x2)
    mdCondLineMax = np.average(mdCondLineUncer, axis=0) + np.std(mdCondLineUncer, axis=0, ddof=1)
    mdCondLineMin = np.average(mdCondLineUncer, axis=0) - np.std(mdCondLineUncer, axis=0, ddof=1)
    mdVacLine = mdVFit(averageFit, x2)
    mdVacLineMax = np.average(mdVacLineUncer, axis=0) + np.std(mdVacLineUncer, axis=0, ddof=1)
    mdVacLineMin = np.average(mdVacLineUncer, axis=0) - np.std(mdVacLineUncer, axis=0, ddof=1)
    gcmcLine = gcmcCFit(averageFit, x3)
    gcmcLineMax = np.average(gcmcLineUncer, axis=0) + np.std(gcmcLineUncer, axis=0, ddof=1)
    gcmcLineMin = np.average(gcmcLineUncer, axis=0) - np.std(gcmcLineUncer, axis=0, ddof=1)

    # Points
    point1 = plt.errorbar(x[:4], mdCond, yerr=mdCondUncer, fmt='bo', mec='c', ecolor='k')
    point2 = plt.errorbar(x[:3], mdVac, yerr=mdVacUncer, fmt='rs', mec='r', ecolor='k')
    point3 = plt.errorbar(x, gcmcCond, yerr=gcmcCondUncer, linestyle='None', marker='v', color='grey', mec='grey', ecolor='k')
    # Lines
    plt.plot(x2, mdCondLine, 'b--')
    plt.plot(x2, mdVacLine, 'r--')
    plt.plot(x3, gcmcLine, linestyle='dashed', color='grey')
    # Line uncertainty
    plt.fill_between(x3, mdCondLineMax, mdCondLineMin, color='b', alpha=0.2)
    plt.fill_between(x3, mdVacLineMax, mdVacLineMin, color='r', alpha=0.2)
    plt.fill_between(x3, gcmcLineMax, gcmcLineMin, color='grey', alpha=0.2)
    # Straight line
    plt.axhline(averageFit[0], color='k')

# Labels
plt.xlabel('1/L (1/nm)')
plt.ylabel('$\mu$ (kJ/mol)')
#plt.legend((point1, point2, point3), ('MD Conducting BC', 'MD Vacuum BC', 'GCMC Conducting BC'), loc='upper left', numpoints=1)
plt.savefig('allExtrap.png')
plt.show()
