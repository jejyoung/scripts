import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt

# Calculate uncertainty in fit for extrapolation slope
# Input: concentration.dat mdC.dat mdV.dat gcmc.dat
# Output: Fit parameters and figure extrapSlope.png
# Fit: A*exp(B*m) (for MDC, GCMC)
#      A*exp(B*m) + C*exp(D*m) (for MDV)


# Fitting function
# Parameter order: [A-mdC, B-mdC, A-mdV, B-mdV, C-mdV, D-mdV, A-gcmc, B-gcmc, A-mdCExpl, B-mdCExpl]
def mdCFit(params, m):
    return params[0]*np.exp(params[1]*m)
def mdVFit(params, m):
    return params[0]*np.exp(params[1]*m) + params[2]*np.exp(params[3]*m)
def gcmcCFit(params, m):
    return params[0]*np.exp(params[1]*m)
def mdCFitExplicit(params, m):
    return params[0]*np.exp(params[1]*m)

def toMinimize(params, m, toFit, funct):
    fit = funct(params, m)
    return np.sum((toFit-fit)**2)

def minimizeAll(guess, m, mExpl, mdC, mdV, gcmcC, mdCExpl):
    result1 = minimize(toMinimize, guess[0:2], args=(m, mdC, mdCFit), method='Nelder-Mead').x
    result2 = minimize(toMinimize, guess[2:6], args=(m, mdV, mdVFit), method='Nelder-Mead').x
    result3 = minimize(toMinimize, guess[6:8], args=(m, gcmcC, gcmcCFit), method='Nelder-Mead').x
    result4 = minimize(toMinimize, guess[8:10], args=(mExpl, mdCExpl, mdCFitExplicit), method='Nelder-Mead').x
    return np.concatenate((result1, result2, result3, result4))


# Data
m = np.loadtxt('concentration.dat')
mExplicit = np.loadtxt('concExplicit.dat')
mdCond, mdCondUncer = np.loadtxt('mdC.dat', unpack=True)
mdVac, mdVacUncer = np.loadtxt('mdV.dat', unpack=True)
gcmcCond, gcmcCondUncer = np.loadtxt('gcmc.dat', unpack=True)
mdCondExplicit, mdCondExplicitUncer = np.loadtxt('mdCExplicit.dat', unpack=True)

# Fit once to determine parameter guesses
guess = minimizeAll([-4, -17, -4, -13, 1.5, -2, -4, -18, -4, -17], m, mExplicit, mdCond, mdVac, gcmcCond, mdCondExplicit)

# Loop through Nsamples
Nsample = 1000
fit = np.zeros((Nsample, 10))
x = np.linspace(0, 5.5, 100)
mdCondLineUncer = np.zeros((Nsample, x.size))
mdVacLineUncer = np.zeros((Nsample, x.size))
gcmcLineUncer = np.zeros((Nsample, x.size))
mdCExplLineUncer = np.zeros((Nsample, x.size))
for i in range(Nsample):
    # Randomly select dataset
    mdC = np.random.normal(mdCond, mdCondUncer)
    mdV = np.random.normal(mdVac, mdVacUncer)
    gcmcC = np.random.normal(gcmcCond, gcmcCondUncer)
    mdCExpl = np.random.normal(mdCondExplicit, mdCondExplicitUncer)
    # Calculate parameter values
    param = minimizeAll(guess, m, mExplicit, mdC, mdV, gcmcC, mdCExpl)
    # Add parameters to list
    fit[i,:] = param
    # Add fit to line uncertainty
    mdCondLineUncer[i,:] = mdCFit(param[0:2], x)
    mdVacLineUncer[i,:] = mdVFit(param[2:6], x)
    gcmcLineUncer[i,:] = gcmcCFit(param[6:8], x)
    mdCExplLineUncer[i,:] = mdCFitExplicit(param[8:10], x)
# Calculate average and standard deviation
averageFit = np.average(fit, axis=0)
medianFit = np.median(fit, axis=0)
stDevFit = np.std(fit, axis=0, ddof=1)
np.set_printoptions(linewidth=120, suppress=True)
print("The average is " + repr(averageFit))
print("The median is " + repr(medianFit))
print("The standard deviation is " + repr(stDevFit))
print("The guess was " + repr(guess))

# Plot output
# Create lines
mdCondLine = mdCFit(medianFit[0:2], x)
mdCondLineMax = np.average(mdCondLineUncer, axis=0) + np.std(mdCondLineUncer, axis=0, ddof=1)
mdCondLineMin = np.average(mdCondLineUncer, axis=0) - np.std(mdCondLineUncer, axis=0, ddof=1)
mdVacLine = mdVFit(medianFit[2:6], x)
mdVacLineMax = np.average(mdVacLineUncer, axis=0) + np.std(mdVacLineUncer, axis=0, ddof=1)
mdVacLineMin = np.average(mdVacLineUncer, axis=0) - np.std(mdVacLineUncer, axis=0, ddof=1)
gcmcLine = gcmcCFit(medianFit[6:8], x)
gcmcLineMax = np.average(gcmcLineUncer, axis=0) + np.std(gcmcLineUncer, axis=0, ddof=1)
gcmcLineMin = np.average(gcmcLineUncer, axis=0) - np.std(gcmcLineUncer, axis=0, ddof=1)
mdCondExplLine = mdCFitExplicit(medianFit[8:10], x)
mdCondExplLineMax = np.average(mdCExplLineUncer, axis=0) + np.std(mdCExplLineUncer, axis=0, ddof=1)
mdCondExplLineMin = np.average(mdCExplLineUncer, axis=0) - np.std(mdCExplLineUncer, axis=0, ddof=1)

plt.figure(figsize=(5, 3.75))
# Points
point1 = plt.errorbar(m**.5, mdCond, yerr=mdCondUncer, fmt='bo', mec='b', ecolor='k', capsize=2)
point2 = plt.errorbar(m**.5, mdVac, yerr=mdVacUncer, fmt='rs', mec='r', ecolor='k', capsize=2)
point3 = plt.errorbar(m**.5, gcmcCond, yerr=gcmcCondUncer, linestyle='None', marker='v', color='grey', mec='grey', ecolor='k', capsize=2)
point4 = plt.errorbar(mExplicit**.5, mdCondExplicit, yerr=mdCondExplicitUncer, fmt='g*', mec='g', ecolor='k', capsize=2)
# Theoretical point
point5 = plt.errorbar(0, -5.399889536, fmt='kx', markersize=10)
# Lines
plt.plot(x**.5, mdCondLine, 'b--')
plt.plot(x**.5, mdVacLine, 'r--')
plt.plot(x**.5, gcmcLine, linestyle='dashed', color='grey')
#plt.plot(x, mdCondExplLine, 'g--')
# Line uncertainty
plt.fill_between(x**.5, mdCondLineMax, mdCondLineMin, color='b', alpha=0.2)
plt.fill_between(x**.5, mdVacLineMax, mdVacLineMin, color='r', alpha=0.2)
plt.fill_between(x**.5, gcmcLineMax, gcmcLineMin, color='grey', alpha=0.2)
#plt.fill_between(x, mdCondExplLineMax, mdCondExplLineMin, color='g', alpha=0.2)
# Zero line
plt.axhline(color='k')
# Labels
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams.update({'font.size': 10})
plt.rc('text', usetex=True)
csfont = {'fontname':'Times New Roman'}
ax = plt.gca()
for tick in ax.get_xticklabels():
    tick.set_fontname("Times New Roman")
for tick in ax.get_yticklabels():
    tick.set_fontname("Times New Roman")
plt.xlim(xmin=0)
plt.xlabel('$\sqrt{Concentration}$', **csfont)
plt.ylabel('$a$ (kJnm/mol)', **csfont)
plt.legend((point1, point2, point3, point4, point5), ('MD Conducting BC', 'MD $\epsilon_{\infty}=73$ BC', 'GCMC Conducting BC', 'Explict MD Conducting BC', 'Theoretical Slope'), loc='lower right', numpoints=1, fontsize=8)
plt.savefig('extrapSlopeBig.pdf', bbox_inches='tight')
plt.show()

