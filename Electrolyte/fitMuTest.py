import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize

def fit(params, x, A):
    lngam = -A*np.sqrt(3*x)/(1+params[1]*np.sqrt(x)) + params[2]*x + params[3]*x**2 + params[4]*x**3
    return params[0] + 3*R*T*np.log(x)+3*R*T*lngam+R*T*np.log(4)
def fit2(params, x, A):
    lngam = -A*np.sqrt(3*x)/(1+params[1]*np.sqrt(x)) + params[2]*x + params[3]*x**2# + params[4]*x**3
    return params[0] + 3*R*T*np.log(x)+3*R*T*lngam+R*T*np.log(4)
def fit3(params, x, A):
    lngam = -A*np.sqrt(3*x)/(1+params[1]*np.sqrt(x)) + params[2]*x# + params[3]*x**2 + params[4]*x**3
    return params[0] + 3*R*T*np.log(x)+3*R*T*lngam+R*T*np.log(4)
def toMinimize(params, m, sims, A):
    return np.sum((sims-fit(params, m, A))**2)
def toMinimize2(params, m, sims, A):
    return np.sum((sims-fit2(params, m, A))**2)
def toMinimize3(params, m, sims, A):
    return np.sum((sims-fit3(params, m, A))**2)

m, mu = np.loadtxt('model1Mu.dat', unpack=True)

e = 1.6021766E-19 #C
Na = 6.02214E23
dens = 999.00 # kg/m3
eps = 73
eps0 = 8.85418782E-12 #C2/Jm
kb = 1.38064852E-23 #J/K
T = 298.15 #K
A = 2*e**3*(2*np.pi*Na*dens)**0.5/(4*np.pi*eps*eps0*kb*T)**1.5
#A = (.75*1.5)*e**3*(2*np.pi*Na*dens)**0.5/(4*np.pi*eps*eps0*kb*T)**1.5
R = 0.008134462618 #kJ/molK
#meth = 'BFGS'
meth = 'Nelder-Mead'
#meth = 'Powell'
fit1 = minimize(toMinimize, [-180, 1, 0.01, 0.01, 0.01], args = (m, mu, A), method = meth).x
fitp2 = minimize(toMinimize2, [-180, 1, 0.01, 0.01], args = (m, mu, A), method = meth).x
fitp3 = minimize(toMinimize3, [-180, 1, 0.01], args = (m, mu, A), method = meth).x
#fit1 = minimize(toMinimize, [-600, 1, 0.1, 0.01, 0.01], args = (m, mu, A), method = meth).x
#fitp2 = minimize(toMinimize2, [-600, 1, 0.1, 0.01], args = (m, mu, A), method = meth).x
#fitp3 = minimize(toMinimize3, [-600, 1, 0.1], args = (m, mu, A), method = meth).x
print(fit1)
print(fitp2)
print(fitp3)
mfit = np.linspace(0.05,7,100)
fitMu = fit(fit1, mfit, A)
fitMu2 = fit2(fitp2, mfit, A)
fitMu3 = fit3(fitp3, mfit, A)
plt.figure(figsize=(3.25, 2.4375))
plt.plot(m, mu, 'bo')
plt.plot(mfit, fitMu, 'b-', label='eqn. 3.9')
plt.plot(mfit, fitMu2, 'b--', label='eqn. 3.10')
plt.plot(mfit, fitMu3, 'b:', label='eqn. 3.11')
plt.xlim(xmin=0)
plt.xlabel('$m$/mol$\cdot$kg$^{-1}$')
plt.ylabel('$\mu_{\mathrm{CaCl}_2}$/kJ$\cdot$mol$^{-1}$')
plt.legend(fancybox=False, frameon=False)
plt.savefig('muRefFit.pdf', bbox_inches='tight')
plt.show()
